import CartParser from './CartParser';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.

	it("should successfully parse lines", () => {
		const lineToParse = "Scelerisque lacinia,18.90,1";

		const newLine = parser.parseLine(lineToParse);

		expect(typeof newLine).toBe('object');
		expect(Object.keys(newLine)).toContain('id');
		expect(Object.keys(newLine)).toContain('name');
		expect(Object.keys(newLine)).toContain('price');
		expect(Object.keys(newLine)).toContain('quantity');
		expect(newLine.name).toBe("Scelerisque lacinia");
		expect(newLine.price).toBe(18.9);
		expect(newLine.quantity).toBe(1);
	});

	it("should properly count total price", () => {
		const items = [
			{
				id: 1,
				price: 10,
				quantity: 2
			},
			{
				id: 2,
				price: 20,
				quantity: 3
			}, 
			{
				id: 3,
				price: 30,
				quantity: 4
			}
		];

		const total = parser.calcTotal(items);

		expect(total).toBe(200);
	});

	it("should create error object", () => {
		const type = 'cell';
		const row = 2;
		const column = 2;
		const message = "test error message";

		const error = parser.createError(type, row, column, message);

		expect(typeof error).toBe('object');
		expect(Object.keys(error)).toContain('type');
		expect(Object.keys(error)).toContain('row');
		expect(Object.keys(error)).toContain('column');
		expect(Object.keys(error)).toContain('message');
		expect(error.type).toBe(type);
		expect(error.row).toBe(row);
		expect(error.column).toBe(column);
		expect(error.message).toBe(message);
	});

	it("should return empty error array when validation is successful", () => {
		const csvToValidate = `Product name,Price,Quantity
							   Mollis consequat,9.00,2`;
		const errors = parser.validate(csvToValidate);

		expect(errors.length).toBe(0);
	});
	
	it("should return header error when invalid headers provided", () => {
		const csvToValidate = `Product Name,Price,Quantity
							   Mollis consequat,9.00,2`;
		const errors = parser.validate(csvToValidate);
		let headerPresent = false;
		errors.forEach(error => {
			if (error.type === 'header') {
				if (error.message.includes('Expected header to be named "Product name" but received Product Name.')) {
					headerPresent = true;
				}
			}
		})

		expect(errors.length).toBeGreaterThan(0);
		expect(headerPresent).toBe(true);
	});

	it("should return row error when invalid column number provided", () => {
		const csvToValidate = `Product name,Price,Quantity
							   Mollis consequat,9.00`;

		const errors = parser.validate(csvToValidate);
		let headerPresent = false;
		errors.forEach(error => {
			if (error.type === 'row') {
				if (error.message.includes("Expected row to have")) {
					headerPresent = true;
				}
			}
		})

		expect(errors.length).toBeGreaterThan(0);
		expect(headerPresent).toBe(true);
	});

	it("should return cell error when empty string provided", () => {
		const csvToValidate = `Product name,Price,Quantity
							   ,9.00,2`;

		const errors = parser.validate(csvToValidate);
		let headerPresent = false;
		errors.forEach(error => {
			if (error.type === 'cell') {
				if (error.message.includes('Expected cell to be a nonempty string but received "".')) {
					headerPresent = true;
				}
			}
		})

		expect(errors.length).toBeGreaterThan(0);
		expect(headerPresent).toBe(true);
	});

	it("should return cell error when invalid number provided", () => {
		const csvToValidate = `Product name,Price,Quantity
							   Mollis consequat,-9.00,2`;
		const csvToValidateNan = `Product name,Price,Quantity
							   Mollis consequat,abcd,2`;

		const errors = parser.validate(csvToValidate);
		const errors_nan = parser.validate(csvToValidateNan);
		let errorPresent = false;
		errors.forEach(error => {
			if (error.type === 'cell') {
				if (error.message.includes('Expected cell to be a positive number but received "-9.00".')) {
					errorPresent = true;
				}
			}
		});
		let errorPresent_nan = false;
		errors_nan.forEach(error => {
			if (error.type === 'cell') {
				if (error.message.includes('Expected cell to be a positive number but received "abcd"')) {
					errorPresent_nan = true;
				}
			}
		});

		expect(errors.length).toBeGreaterThan(0);
		expect(errors_nan.length).toBeGreaterThan(0);
		expect(errorPresent).toBe(true);
		expect(errorPresent_nan).toBe(true);
	});

	it("should return multiple errors when invalid csv provided", () => {
		const csvToValidate = `Product Name,Price,Quantity
							   ,-9.00,2`;

		const errors = parser.validate(csvToValidate);
		let headerErrorPresent = false;
		let cellErrorPresentString = false;
		let cellErrorPresentNumber = false;
		errors.forEach(error => {
			if (error.type === 'header') {
				if (error.message.includes('Expected header to be named "Product name" but received Product Name.')) {
					headerErrorPresent = true;
				}
			}
			if (error.type === 'cell') {
				if (error.message.includes('Expected cell to be a nonempty string but received "".')) {
					cellErrorPresentString = true;
				}
				if (error.message.includes('Expected cell to be a positive number but received "-9.00".')) {
					cellErrorPresentNumber = true;
				}
			}
		});

		expect(errors.length).toBe(3);
		expect(headerErrorPresent).toBe(true);
		expect(cellErrorPresentString).toBe(true);
		expect(cellErrorPresentNumber).toBe(true);
	});

	it("should throw an error when validation fails", () => {
		parser.readFile = jest.fn().mockReturnValue(`Product name,Price,Quantity
													 Mollis consequat,9.00,2
													 Tvoluptatem,10.32,-9`);
		expect(() => parser.parse("")).toThrow();
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it("should return json with all data and total count", () => { 
		parser.readFile = jest.fn().mockReturnValue(`Product name,Price,Quantity
													 Mollis consequat,9.00,2
													 Tvoluptatem,10.32,1
													 Scelerisque lacinia,18.90,1
													 Consectetur adipiscing,28.72,10
													 Condimentum aliquet,13.90,1`);

		const data = parser.parse("");

		expect(typeof data).toBe('object');
		expect(Object.keys(data)).toContain('items');
		expect(Object.keys(data)).toContain('total');
		expect(Array.isArray(data.items)).toBe(true);
		expect(typeof data.total).toBe('number');
		expect(data.items.length).toBe(5);
		expect(data.total).toBe(348.32);
		expect(Object.keys(data.items[0])).toContain('id');
		expect(Object.keys(data.items[0])).toContain('name');
		expect(Object.keys(data.items[0])).toContain('price');
		expect(Object.keys(data.items[0])).toContain('quantity');
	});
});